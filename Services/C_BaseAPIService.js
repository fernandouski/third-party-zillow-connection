/**
 * @constructor
 */
C_BaseAPIService = function () {
    this.m_strApiKey = "";
};

/**
 * Function will set the API Key.
 * @param {String} p_strAPIKey   - API Key
 */
C_BaseAPIService.prototype.mf_setAPIKey = function (p_strAPIKey) {
    this.m_strApiKey = p_strAPIKey;
};

module.exports = C_BaseAPIService;