var Request = require("request");
var C_BaseAPIService = require("./C_BaseAPIService");
var C_ZillowDto = require("./../models/DTOs/CZillowDto");

/**
 * @constructor
 */
C_ZillowService = function () {
    this.m_oHeaderForZillowAPI = {
        url: "http://www.zillow.com/webservice/GetSearchResults.htm"
    }
};

// Inherit properties from the base API Service Class
C_ZillowService.prototype = new C_BaseAPIService();

/**
 * Function will call the Zillow API "http://www.zillow.com/webservice/GetSearchResults.htm endpoint
 * @param {C_ZillowDto} p_ZillowDto     - DTO containing information about the request.
 * @param {Function} pf_handleResponse  - Function callback that will handle the response.
 */
C_ZillowService.prototype.mf_getSearchResults = function (p_ZillowDto, pf_handleResponse) {
    //Add parameters to the query string portion of the header.
    this.m_oHeaderForZillowAPI['qs'] = {
        'zws-id': this.m_strApiKey,
        'address': decodeURIComponent(p_ZillowDto.m_address.replace(/%20/g, "+")),
        'citystatezip': decodeURIComponent(p_ZillowDto.m_zipcodeCityState.replace(/%20/g, "+"))
    };
    // The endpoint requires a GET request.
    this.m_oHeaderForZillowAPI['method'] = "GET";
    Request(this.m_oHeaderForZillowAPI, pf_handleResponse);
};

module.exports = C_ZillowService;