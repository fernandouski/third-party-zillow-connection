/**
 *
 * @property {string}  m_zillowAPIKey     - The unique Zillow API key to make query their API.
 */
var configuration = {
    m_zillowAPIKey: process.env.APP_ZILLOW_API_KEY || ""
};

module.exports = configuration;