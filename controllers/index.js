var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  console.log("we will render the view here or the app. ");
  res.render('index', { title: 'Express' });
});

module.exports = router;
