var express = require('express');
var router = express.Router();

var C_ZillowService = require("./../Services/C_ZillowService");
var O_Configuration = require("./../config/config");
var C_ZillowDto = require("./../models/DTOs/CZillowDto");
var O_xmlJsonParser = require('xml2js').parseString;

/**
 * This Route will call the zillow api and respond with the result.
 */
router.get('/HomeResults', function (req, res, next) {

    // I want to allow the requests from multiple servers.
    res.setHeader('Access-Control-Allow-Origin', '*');

    /**
     * Function callback handles sends out the response in json format to the client.
     * @type {function(this:*)}
     */
    var lf_parserCallback = function (err, result) {
        res.send(result);
    }.bind(this);

    /**
     * Function handles the response to the Zillow api and transfrom
     * the xml result to a json object.
     * @type {function(this:*)}
     */
    var lf_handleResponse = function (p_oError, p_oResponse, p_oBody) {
        O_xmlJsonParser(p_oBody, {explicitArray: false}, lf_parserCallback);
    }.bind(this);

    var l_oZillowService = new C_ZillowService();
    var l_dtoZillow = new C_ZillowDto();

    l_oZillowService.mf_setAPIKey(O_Configuration.m_zillowAPIKey);
    l_dtoZillow.mf_toDtoFromObject(req.query);

    l_oZillowService.mf_getSearchResults(l_dtoZillow, lf_handleResponse);
});

module.exports = router;