var C_BaseDto = require("./CBaseDto");

C_ZillowDto = function () {
    this.m_zipcodeCityState = "";
    this.m_address = "";
    this.m_rentZestimate = false;
    return this;
};

// Inherit the properties of the base dto
C_ZillowDto.prototype = new C_BaseDto();

C_ZillowDto.prototype.m_transformationProperties = {
    address: "m_address",
    citystatezip: "m_zipcodeCityState",
    rentzestimate: "m_rentZestimate"
};

module.exports = C_ZillowDto;